const browserSync = require('browser-sync'),
    cssSort = require('css-declaration-sorter'),
    del = require('del'),
    gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    babel = require('gulp-babel'),
    cache = require('gulp-cache'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    notify = require('gulp-notify'),
    postCss = require('gulp-postcss'),
    rename = require('gulp-rename'),
    scss = require('gulp-sass'),
    uglify = require('gulp-uglify');

gulp.task('browser-sync', () => {
  browserSync({
    server: {
      baseDir: 'app',
    },
    notify: false,
    online: false,
  });
});

gulp.task('js', () => {
  return gulp.src(['app/js/app.js']).
      pipe(babel({
        presets: ['env'],
      })).
      pipe(concat('app.min.js')).
      pipe(uglify()).
      pipe(gulp.dest('app/js')).
      pipe(browserSync.reload({stream: true}));
});

gulp.task('scss', () => {
  return gulp.src('app/scss/**/*.scss').
      pipe(scss({outputStyle: 'expand'}).on('error', notify.onError())).
      pipe(rename({suffix: '.min', prefix: ''})).
      pipe(autoprefixer(['last 6 versions'])).
      pipe(postCss([cssSort({order: 'smacss'})])).
      pipe(cleanCSS()).
      pipe(gulp.dest('app/css')).
      pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['scss', 'js', 'browser-sync'], () => {
  gulp.watch('app/scss/**/*.scss', ['scss']);
  gulp.watch('app/js/app.js', ['js']);
  gulp.watch('app/**/*.html', browserSync.reload);
});

gulp.task('imagemin', () => {
  return gulp.src('app/img/**/*').
      pipe(imagemin()).
      pipe(gulp.dest('dist/img'));
});

gulp.task('build', ['removedist', 'imagemin', 'scss', 'js'], () => {
  let buildFiles = gulp.src('app/**/*.html').pipe(gulp.dest('dist'));

  let buildCss = gulp.src('app/css/**/*.css').pipe(gulp.dest('dist/css'));

  let buildJs = gulp.src('app/js/**/*.js').pipe(gulp.dest('dist/js'));

  let buildFonts = gulp.src('app/fonts/**/*').pipe(gulp.dest('dist/fonts'));
});

gulp.task('removedist', () => {
  return del.sync('dist');
});

gulp.task('clearcache', () => {
  return cache.clearAll();
});

gulp.task('default', ['watch']);