'use strict';

let productTrigger = item => {
  if (item.classList.contains('active')) {
    item.classList.remove('active');
  } else {
    item.classList.add('active');
  }
};

[].forEach.call(document.querySelectorAll('.product-item'), item => {
  if (!item.classList.contains('disabled')) {
    let mouseOutEvent = (currentTarget, item) => {
      currentTarget.addEventListener('mouseleave', function call(e) {
        productTrigger(item);
        e.target.removeEventListener(e.type, call);
      });
    };

    item.querySelector('.product-link a').addEventListener('click',
        e => mouseOutEvent(e.currentTarget,
            e.currentTarget.parentNode.parentNode.parentNode.parentNode));

    item.querySelector('.card').addEventListener('click',
        e => mouseOutEvent(e.currentTarget, e.currentTarget.parentNode));
  }
})
;